var db;

var comma = d3.format(",");

var x_bound = 960, y_bound = 250;
var mgn = { t:0, r:5, b:0, l:5 };

var height=y_bound - mgn.t - mgn.b,
    width=x_bound - mgn.l - mgn.r;

var div = d3.select("#vis");

var svg = div.append("svg")
             .attr("width", width)
             .attr("height", height)
             .append("g")
             .attr("transform", "translate(" + mgn.l + "," + mgn.t + ")");

var scale_x_discrete = d3.scaleBand().range([width, 0]);
var scale_y_continuous = d3.scaleLinear().range([height, 0]);
var scale_color_race = d3.scaleOrdinal(d3.schemeCategory10);

var race_ord = d3.scaleOrdinal().range([0, 1, 2, 3])
                 .domain(["Black", "White", "Latino", "Other"]);

function compare_years(a, b) { return((+a.yrs) - (+b.yrs)); }

function compare_race(a, b) { return(race_ord(a.Race) - race_ord(b.Race)); }

function compare_race_yrs(a, b) { return(compare_race(a, b) || compare_years(a, b)); }

function compare_convicted(a, b) {
  return((+a.Convicted) - (+b.Convicted) || compare_race(a, b) || compare_years(a, b));
}

function compare_exonerated(a, b) {
  return((+a.Exonerated) - (+b.Exonerated) || compare_race(a, b) || compare_years(a, b));
}

function compare_state(a, b) {
  return(a.State.localeCompare(b.State) || compare_race(a, b) || compare_years(a, b));
}

var filter_category = "all";
var filter_reason = "innocent";
var sort_by = "years";

function update_app(dat) {

  scale_color_race = scale_color_race.domain(d3.set(dat, d => d.Race).values().sort());

  var t1 = d3.transition().delay(350).ease(d3.easeQuad).duration(1250);

  if (filter_category != "all") {
    dat = dat.filter(d => d.Race == filter_category);
  }

  if (filter_reason != "innocent") {
    dat = dat.filter(d => d.Reason == filter_reason);
  }

  var y_min = d3.min(dat, d => (d.mhalf));
  var y_max = d3.max(dat, d => (d.half));

  if (sort_by === "convicted") {
    dat = dat.sort(compare_convicted).reverse();
  } else if (sort_by === "exonerated") {
    dat = dat.sort(compare_exonerated).reverse();
  } else if (sort_by === "years") {
    dat = dat.sort(compare_years).reverse();
  } else if (sort_by === "both") {
    dat = dat.sort(compare_race_yrs).reverse();
  } else if (sort_by === "state") {
    dat = dat.sort(compare_state).reverse();
  }

  d3.select("#answer").html(comma(dat.length));

  scale_x_discrete = scale_x_discrete.domain(dat.map(d => d.Name));
  scale_y_continuous = scale_y_continuous.domain([y_min, y_max]);

  vg.remove();
  svg.selectAll(".segment").remove();

  var segments = svg.selectAll(".segment").data(dat);

  segments.enter().append("line")
    .attr("class", d => "segment " + d.Name.replace(/\s/g, ''))
    .attr("y1", d => scale_y_continuous(-20))
    .attr("y2", d => scale_y_continuous(20))
    .transition(t1)
    .attr("x1", d => scale_x_discrete(d.Name))
    .attr("x2", d => scale_x_discrete(d.Name))
    .attr("stroke", d => scale_color_race(d.Race))
    .attr("stroke-width", 1)
    .transition(t1)
    .attr("y1", d => scale_y_continuous(d.mhalf))
    .attr("y2", d => scale_y_continuous(d.half))


  segments.exit().remove();

  var voronoi = d3.voronoi()
    .x(d => scale_x_discrete(d.Name))
    .y(0)
    .extent([ [-mgn.l, -mgn.t], [width + mgn.r, height + mgn.b] ]);

  vg = svg.append("g").attr("class", "voronoi");

  vg.selectAll("path")
    .data(voronoi.polygons(dat))
    .enter().append("path")
    .attr("d", function(d) { return d ? "M" + d.join("L") + "Z" : null; })
    .on("click", d => window.open("https://deathpenaltyinfo.org/innocence-cases#" + d.data.ref, "dpinfo"))
    .on("mouseover", mouseover)
    .on("mouseout", mouseout);

  function mouseover(d) {

    d3.select("." + d.data.Name.replace(/\s/g, '')).attr("stroke-width", "2");

    var p = d.data;
    d3.select("#pname").html("<b>" + p.Name + "</b> (" + p.State + ")").transition().duration(100);
    d3.select("#prace").html(p.Race);
    d3.select("#preason").html(p.Reason);
    d3.select("#pspan").html(p.Convicted + " - " + p.Exonerated);

  }

  function mouseout(d) {
    d3.select("." + d.data.Name.replace(/\s/g, '')).attr("stroke-width", "1");
    d3.select("#pname").html("&nbsp;");
    d3.select("#prace").html("&nbsp;");
    d3.select("#preason").html("&nbsp;");
    d3.select("#pspan").html("&nbsp;");
  }

}

var vg = svg.append("g");

function start_app(err, dat) {
  db = dat;
  update_app(dat);
}

function change_category() { filter_category = d3.select(this).property('value'); update_app(db); }
function change_reason() { filter_reason = d3.select(this).property('value'); update_app(db); }
function change_sort() { sort_by = d3.select(this).property('value'); update_app(db); }

var sel_category = d3.select("#sel_category").on("change", change_category);
var sel_reason = d3.select("#sel_reason").on("change", change_reason);
var sel_sort = d3.select("#sel_sort").on("change", change_sort);

d3.queue()
  .defer(d3.json, "data/innocence.json")
  .await(start_app);
